using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public AudioMixer mixer;

    // Start is called before the first frame update
    void Start()
    {
        //mixer.SetFloat("chorusdepth", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            AudioMixerSnapshot muffled = mixer.FindSnapshot("Muffled");
            muffled.TransitionTo(1.5f);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            AudioMixerSnapshot normal = mixer.FindSnapshot("Normal");
            normal.TransitionTo(1.5f);
        }
    }
}
