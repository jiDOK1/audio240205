using UnityEngine;

public class Floor : MonoBehaviour
{
    [SerializeField] FloorType floorType;
    public int Index => (int)floorType;
}

public enum FloorType
{
    Leaves, Puddle, Inside
}
