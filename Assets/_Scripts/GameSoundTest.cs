using UnityEngine;

public class GameSoundTest : MonoBehaviour
{
    public AudioClip[] clips;
    AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            source.PlayOneShot(clips[0]);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            source.PlayOneShot(clips[1]);
        }
    }
}
