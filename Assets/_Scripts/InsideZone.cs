using UnityEngine;
using UnityEngine.Audio;

public class InsideZone : MonoBehaviour
{
    public AudioMixer mixer;
    public string insideName;
    public string outsideName;
    AudioMixerSnapshot outside;
    AudioMixerSnapshot inside;
    int counter;

    void Start()
    {
        outside = mixer.FindSnapshot(outsideName);
        inside = mixer.FindSnapshot(insideName);
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        if (counter == 0)
        {
            inside.TransitionTo(0.5f);
        }
        counter++;
    }

    void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        counter--;
        if (counter == 0)
        {
            outside.TransitionTo(0.5f);
        }
    }
}
