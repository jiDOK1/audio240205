using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public StepSounds[] stepSounds;
    public float stepSpeed = 1f;
    AudioSource source;
    CharacterController cc;
    int curIdx;
    float stepTimer;

    void Start()
    {
        source = GetComponent<AudioSource>();
        cc = GetComponent<CharacterController>();
    }

    void Update()
    {
        float finalStepSpeed = Input.GetButton("Run") ? stepSpeed * 1.5f : stepSpeed;
        stepTimer += Time.deltaTime * finalStepSpeed;
        if (stepTimer >= 1f && cc.velocity.sqrMagnitude > 0f)
        {
            stepTimer = 0;
            Ray ray = new Ray(transform.position, Vector3.down);
            RaycastHit hitInfo = new RaycastHit();
            bool hitFloor = Physics.Raycast(ray, out hitInfo, 1.1f);
            if (hitFloor)
            {
                Floor floor = hitInfo.collider.GetComponent<Floor>();
                if (floor != null) curIdx = floor.Index;
                int rndIdx = Random.Range(0, stepSounds[curIdx].steps.Length);
                stepTimer = 0f;
                source.PlayOneShot(stepSounds[curIdx].steps[rndIdx]);
            }
        }
    }
}

[System.Serializable]
public class StepSounds
{
    public AudioClip[] steps;
}

